# Worlds Without Number for Foundry VTT (Unofficial)
Everything you need to play Worlds Without Number in Foundry VTT. This was forked from v1.1.2 of the Old School Essentials project developed by U~man. I have managed to mangle his beautiful project into something that works for Worlds Without Number. All praise should be directed toward him. Any bugs or mistakes are undoubtedly my own.

Find the original OSE project by U~man here: https://gitlab.com/mesfoliesludiques/foundryvtt-ose
## Features
* Calculated Readied/Stowed values, including dynamic tracking of currency weight
* Calculates total wealth from coin, bank (stored coin not calculated against encumbrance), and treaure items
* Track weapon tags; hovering over the tag icon or name displays the full tag description
* Track Effort commitment by Art and have class-specific Effort updated automatically
    * Click Tweaks in the character title bar to activate spellcasting and enter caster class(es)
* Visual indicator of health/strain percentage
* Auto-calculate saves for PCs and NPCs alike
* Calculates movement rates based on Readied/Stowed values
    * Can be switched to manual entry
    * Can calculate movement rates for standard WWN rates or B/X movement rates
* Adds attribute bonuses to hit chance, damage, and Shock
* Shock and damage account for attribute bonuses, the Killing Blow warrior ability, and Foci that add skill levels to damage
    * Click Full Warrior in Tweaks menu to activate Killing Blow
    * Skill damage is activated on a per-item basis, due to the variable nature of Foci
* Support for Specialist and other Foci that allow rolling 3d6/4d6 on skill checks
* Distribute XP through the party sheet
    * Assign percentage shares to henchmen, to support silver-as-XP (and custom XP values) for B/X-style play
* GM can easily roll multiple saving throws or apply damage by selecting the appropriate token(s) and clicking the save or damage in an Art, Spell, or weapon's chat card
* GM Tools: quickly generate things from the GM Tables in WWN
    * Currently supports Nation, Government, Society, and History Construction. More will be added in the future.
* Roll Morale and Instinct checks with two clicks
    * Link Instinct tables from the Compendium to each NPC sheet, so the appropriate table is only a click away
* Compendium includes weapons, armor, adventuring gear, arts, spells, and foci. Deluxe edition content is not included.
    * Thanks to Gavin over at Necrotic Gnome, the Compendium now includes OSE spells and monsters.
## TODO
* Add highest Dex mod when using Group Initiative
## License
This Foundry VTT system requires the Worlds Without Number rules, available through the Kickstarter or through sale at some point in the future.

This third party product is not affiliated with or approved by Sine Nomine Publishing.
Worlds Without Number is a trademark of Sine Nomine Publishing.

Old School Essentials spells and monsters used with permission under Open Game License, originally adapted from the greatest role playing game in the world.

## Artwork
Icons are from [Rexxard](https://assetstore.unity.com/packages/2d/gui/icons/flat-skills-icons-82713).